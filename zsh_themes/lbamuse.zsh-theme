# vim:ft=zsh ts=2 sw=2 sts=2

#local return_code="%(?..%{$fg_bold[red]%}%? ↵%{$reset_color%})"
local return_code_prompt="%(?..%{$fg_bold[red]%} ↵ %?)"

PROMPT='
%{$fg_bold[green]%}${PWD/#$HOME/~}%{$reset_color%}$(git_prompt_info) %{$fg_bold[blue]%}⌚ %* ${return_code_prompt}%{$reset_color%}
$ '
#RPS1="${return_code}"

ZSH_THEME_GIT_PROMPT_PREFIX=" on %{$fg[magenta]%}\ue0a0 "
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}!"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[green]%}?"
ZSH_THEME_GIT_PROMPT_CLEAN=""

