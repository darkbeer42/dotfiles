
"-------------------------------------------------
" Plugins using junegunn/vim-plug
" Make sure to use single quotes when calling Plug
call plug#begin('~/.vim/plugged')


" Functional Plugins
Plug 'kien/ctrlp.vim' "Ctrl-P Search tool
Plug 'itchyny/lightline.vim' "Status line


" Themes
Plug 'vim-scripts/wombat256.vim'

" staging

call plug#end()
"-------------------------------------------------
" Color scheme options
"
" to get colors correctly
" alongside with `export TERM=xterm-256color` line in ~/.*sh.rc file.
if !has('gui_running')
	  set t_Co=256
endif

" active colorscheme
colorscheme wombat256mod

"-------------------------------------------------
" Status line options
"
" Shorten default delay for key combination
" help reducing statusline update delay when hitting <ESC>.
set timeoutlen=200

" lightline status line
set laststatus=2 " Otherwise the status line doesn't appear
set noshowmode   " No need to display mode in regular vim status line, it is in lightline status
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ }

"-------------------------------------------------
" ctrlp options
"
let g:ctrlp_max_files = 0 "0 for unlimited
let g:ctrlp_max_depth = 100
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$' "ignore git, svn, mercurial files
let g:ctrlp_use_caching = 0
let g:ctrlp_clear_cache_on_exit = 1

"-------------------------------------------------
" Generic options

set tabstop=4     " tabs are at proper location
set expandtab     " don't use actual tab character (ctrl-v)
set shiftwidth=4  " indenting is 4 spaces
set autoindent    " turns autoindent on
set cindent       " stricter rules for C programs

" split windows are created on bottom right
set splitbelow
set splitright

" text display option
set scrolloff=10 " scroll the window so we can always see 10 lines around the cursor
set cursorline   " highlight the current line


" show whitespace chars
"set list
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()


" toggle search highlighting using <F3>
nnoremap <F3> :set hlsearch!<CR>

set ignorecase " case insensitive
set smartcase  " use case if any caps is used

