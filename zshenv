## If not running interactively, don't do anything
[ -z "${PS1}" ] && return

which most > /dev/null 2>&1 && export PAGER=most

## work customs
WORK_DEFINES="$HOME/documents/repositories/git_sierra/lba/config/bash/work.bash"
if [[ -f $WORK_DEFINES ]]; then
    source $WORK_DEFINES
fi

export PATH=$HOME/bin:/usr/local/bin:$HOME/.local/bin:$PATH

## ensure using/sourcing this file will be succesfull.
true
