#!/bin/sh
# usage: install.sh
# Install dotfiles in $HOME.
#-------------------------------------------------

SCRIPTPATH=`readlink -f "$0"`
FILE=`basename "$SCRIPTPATH"`
BASEDIR=`dirname -- "$SCRIPTPATH"`

DOTFILES=`ls -I README.md -I Makefile -I $FILE $BASEDIR`


doList(){
    echo "Install will:"
    for f in $DOTFILES
    do
        HOME_FILE=$HOME/.$f
        DOTFILE=$BASEDIR/$f
        if test -e "$HOME_FILE";
        then echo "\tback up $HOME_FILE, and replace it with symlink to $DOTFILE"
        else echo "\tcreate symlink in HOME folder to $DOTFILE"
        fi
    done
    return 0
}

doClean(){
    echo "Not implemented yet!"
    return 0
}

doBackUp(){
    for f in $DOTFILES
    do
        HOME_FILE=$HOME/.$f
        BACKUP_FILE=$HOME_FILE.`date +%s`.backup
        if test -e "$HOME_FILE";
        then
            echo "\tBacking up $HOME_FILE to $BACKUP_FILE"
            mv $HOME_FILE $BACKUP_FILE
        fi
    done
    return 0
}

doLink(){
    for f in $DOTFILES
    do
        HOME_FILE=$HOME/.$f
        DOTFILE=$BASEDIR/$f
        echo "\tCreating symlink as $HOME_FILE, to $DOTFILE"
        ln -s $DOTFILE $HOME_FILE 
    done
    return 0
}



case "$1" in
  	'list')
        doList
	    ;;
    'install')
        doBackUp
        doLink
        ;;
    'uninstall')
        doClean
        ;;
	*)
        echo "usage:\n $FILE list|install|uninstall"
	    ;;
esac


