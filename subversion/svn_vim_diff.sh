#!/bin/sh

# script to have svn diff displayed using vimdiff
# either using:
# > svn diff --diff-cmd path/to/this/script [file]
# or as default svn diff command: in ~/.subversion/config, [helpers] section:
# diff-cmd = path/to/this/script

# name for left file window
LEFT_DESC=${3}
# name for right file window
RIGHT_DESC=${5}
LEFT=${6}
RIGHT=${7}

# get extension of file:
FILE_NAME=$(basename "$RIGHT")

# create a symbolic
TMP_FILE=/tmp/${FILE_NAME}
ln -s $LEFT $TMP_FILE

echo ${1} 
echo ${2}
echo ${3}
echo ${4}
echo ${5}
echo ${6}
echo ${7}
#vimdiff $MINE $THEIRS -c ":bo sp $MERGED" -c ":diffthis" -c "setl stl=MERGED | wincmd W | setl stl=THEIRS | wincmd W | setl stl=MINE"
vimdiff $TMP_FILE $RIGHT -c "setlocal statusline=BASE" -c "wincmd W" -c "setlocal statusline=WORKING_COPY"
RES=$?

#clean tmp file
rm $TMP_FILE

exit $RES
