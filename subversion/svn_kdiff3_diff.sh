#!/bin/sh

# script to have svn diff displayed using vimdiff
# either using:
# > svn diff --diff-cmd path/to/this/script [file]
# or as default svn diff command: in ~/.subversion/config, [helpers] section:
# diff-cmd = path/to/this/script

# name for left file window
LEFT_DESC=${3}
# name for right file window
RIGHT_DESC=${5}
LEFT=${6}
RIGHT=${7}

# get extension of file:
FILE_NAME=$(basename "$RIGHT")

# create a symbolic
TMP_FILE=/tmp/${FILE_NAME}
ln -s $LEFT $TMP_FILE

kdiff3 $TMP_FILE $RIGHT
RES=$?

#clean tmp file
rm $TMP_FILE

exit $RES
