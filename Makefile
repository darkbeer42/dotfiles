######
## Install dotfiles in $HOME
## Initially taken from https://github.com/chmduquesne/dotfiles
######


FILES=$(filter-out . .. README.md Makefile,$(wildcard *))
LINKRULES=$(FILES:%=link.%)
REMOVERULES=$(FILES:%=rm.%)
ECHORULES=$(FILES:%=echo.%)
BACKUPRULES=$(FILES:%=backup.%)


all: list

echo.%: %
	@if test -e "$(HOME)/.$<" -o -h "$(HOME)/.$<"; then echo "install/uninstall will act on symbolic link:"; ls -la "$(HOME)/.$<"; \
	else echo 'install will create symbolic link "$(HOME)/.$<"'; fi

# exit 0 is needed not to interrupt the make process as a missing dotfile in HOME folder is not a problem
# test -h is used to check for existing but broken symlinks (those are dereferenced by -e, so -e will fail in that case)
backup.%: %
	@test -e "$(HOME)/.$<" -o -h "$(HOME)/.$<" && { echo "Backing up $(HOME)/.$< to $(HOME)/.$<.`date +%s`.backup"; mv $(HOME)/.$<  $(HOME)/.$<.`date +%s`.backup; }; exit 0;


# Note: ln -s fails if the target (dot)file already exists.
link.%: %
	@echo "Creating the symbolic link $(HOME)/.$<"
	@ln -s $(PWD)/$< $(HOME)/.$<

rm.%: %
	@echo "Removing $(HOME)/.$<"
	@rm -rf $(HOME)/.$<

install: $(BACKUPRULES) $(LINKRULES)
	@echo "Config installed!"

uninstall: $(REMOVERULES)
	@echo "Config uninstalled!"

check: $(BACKUPRULES)

clean: uninstall

list: $(ECHORULES)
	@echo "Use 'make install' or 'make uninstall' to actually modify dotfiles configuration."
